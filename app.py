from flask import Flask
from flask import request
from flask_cors import CORS
import requests
from base64 import decodebytes
import base64
import numpy as np
import cv2
from flask import render_template, send_from_directory

app = Flask(__name__, static_url_path='')
CORS(app, resources=r'/*')



def base64_cv2(base64_str):
    imgString = base64.b64decode(base64_str)
    nparr = np.fromstring(imgString, np.uint8)
    image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    return image

def cv2_base64(image):
    base64_str = cv2.imencode('.jpg',image)[1].tostring()
    base64_str = base64.b64encode(base64_str)
    return base64_str

def get_access_token():
    grant_type = "client_credentials"
    client_id = "6xmFD0GmW04759GEv66R5LUt"
    client_secret = "pXka1blpIUO6XrgGyKK587ALI62IcqwG"
    host = f'https://aip.baidubce.com/oauth/2.0/token?grant_type={grant_type}&client_id={client_id}&client_secret={client_secret}&'
    response = requests.get(host)
    if response:
        return (response.json()["access_token"])
    else:
        return None

def idcard(photo_base64 ,b ):
    access_token = get_access_token()
    request_url = "https://aip.baidubce.com/rest/2.0/ocr/v1/idcard"

    params = {"id_card_side":"front","image":photo_base64}
    request_url = request_url + "?access_token=" + access_token
    headers = {'content-type': 'application/x-www-form-urlencoded'}
    response = requests.post(request_url, data=params, headers=headers)
    if "error_msg" not in response.json().keys():
        data =  response.json()
        if "words_result" in data:
            data = [{k:i["words"]}for k , i in data["words_result"].items()]
            return {"success":True, "payload":data}
        else:
            return {"success":False,"payload":response.json()}
    else:
        return {"success": False, "payload": response.json()}

def train_ticket(photo_base64 ,b ):
    access_token = get_access_token()
    request_url = "https://aip.baidubce.com/rest/2.0/ocr/v1/train_ticket"
    params = {"image": photo_base64}
    request_url = request_url + "?access_token=" + access_token
    headers = {'content-type': 'application/x-www-form-urlencoded'}
    response = requests.post(request_url, data=params, headers=headers)
    if "error_msg" not in response.json().keys():
        data =  response.json()
        print(data)
        if "words_result" in data:
            data = [{k:i} for k , i in data["words_result"].items()]
            return {"success":True, "payload":data}
        else:
            return {"success":False,"payload":response.json()}
    else:
        return {"success": False, "payload": response.json()}


def bankcard(photo_base64 ,b ):
    access_token = get_access_token()
    request_url = "https://aip.baidubce.com/rest/2.0/ocr/v1/train_ticket"
    params = {"image": photo_base64}
    request_url = request_url + "?access_token=" + access_token
    headers = {'content-type': 'application/x-www-form-urlencoded'}
    response = requests.post(request_url, data=params, headers=headers)
    if "error_msg" not in response.json().keys():
        data = response.json()
        if "words_result" in data:
            data = [{k: i} for k, i in data["words_result"].items()]
            return {"success": True, "payload": data}
        else:
            return {"success": False, "payload": response.json()}
    else:
        return {"success": False, "payload": response.json()}
def vat_invoice(photo_base64 ,b ):

    access_token = get_access_token()
    request_url = "https://aip.baidubce.com/rest/2.0/ocr/v1/vat_invoice"
    params = {"image": photo_base64}
    request_url = request_url + "?access_token=" + access_token
    headers = {'content-type': 'application/x-www-form-urlencoded'}
    response = requests.post(request_url, data=params, headers=headers)
    if "error_msg" not in response.json().keys():
        data = response.json()

        if "words_result" in data:
            data = data["words_result"]
            _  = {}
            key_list = ("InvoiceNum",
                        "SellerName",
                        "SellerBank",
                        "InvoiceDate",
                        "PurchaserName",
                        "InvoiceTypeOrg",
                        "Checker",
                        "NoteDrawer",
                        "PurchaserBank",
                        "SellerAddress",
                        "PurchaserAddress",
                        "InvoiceCode",
                        "PurchaserRegisterNum",
                        "TotalAmount",
                        "AmountInWords",
                        "AmountInFiguers",
                        "TotalTax",
                        "InvoiceType",
                        "SellerRegisterNum",
                        "AmountInFiguers"
                        )
            _ = [{k:i} for k, i in data.items() if k in key_list]



            CommodityName = [i for i in data["CommodityName"]]
            CommodityType = [i for i in data["CommodityType"]]
            CommodityUnit = [i for i in data["CommodityUnit"]]
            CommodityNum = [i for i in data["CommodityNum"]]
            CommodityPrice = [i for i in data["CommodityPrice"]]
            CommodityTaxRate = [i for i in data["CommodityTaxRate"]]
            CommodityTax = [i for i in data["CommodityTax"]]
            total_len = len(CommodityType)
            for i in range(total_len):
                data = [CommodityName[i]["word"],
                         CommodityType[i]["word"],
                         CommodityUnit[i]["word"],
                         CommodityNum[i]["word"],
                         CommodityPrice[i]["word"],
                         CommodityTaxRate[i]["word"],
                         CommodityTax[i]["word"]]

                _.append({i:data})


            return {"success": True, "payload": _}

    print(response)
    return None

def Universal_text_recognition(photo_base64,original_photo_base64):
    access_token = get_access_token()
    request_url = "https://aip.baidubce.com/rest/2.0/ocr/v1/general"
    params = {"image": photo_base64,
              "recognize_granularity":"small"}
    request_url = request_url + "?access_token=" + access_token
    headers = {'content-type': 'application/x-www-form-urlencoded'}
    response = requests.post(request_url, data=params, headers=headers)
    if "error_msg" not in response.json().keys():
        data = []
        img = base64_cv2(photo_base64)
        for i in response.json()["words_result"]:
            data.append(i["words"]+ "\n ")
            location = i["location"]
            cv2.rectangle(img, (location["left"], location["top"]),
                          (location["left"] + location["width"], location["top"] + location["height"]), (0, 255, 0), 2)
        base64 = cv2_base64(img)
        base64 = add_base64_header(base64,original_photo_base64)
        return {"success":True,"payload":" ".join(data), "image":base64}
    else:
        return {"success": False, "payload": response.json()}

def General_text_recognition_high_precision_version(photo_base64,original_photo_base64):
    access_token = get_access_token()
    request_url = "https://aip.baidubce.com/rest/2.0/ocr/v1/accurate"
    params = {"image": photo_base64}
    request_url = request_url + "?access_token=" + access_token
    headers = {'content-type': 'application/x-www-form-urlencoded'}
    response = requests.post(request_url, data=params, headers=headers)
    if "error_msg" not in response.json().keys():
        img = base64_cv2(photo_base64)
        data = []
        print(response.json())
        for i in response.json()["words_result"]:
            data.append(i["words"] + "\n ")
            location = i["location"]
            cv2.rectangle(img, (location["left"], location["top"]),
                          (location["left"] + location["width"], location["top"] + location["height"]), (0, 255, 0), 2)
        print(data)
        base64 = cv2_base64(img)
        base64 = add_base64_header(base64, original_photo_base64)
        return {"success": True, "payload": " ".join(data), "image":base64}
    else:
        return {"success": False, "payload": response.json()}

def add_base64_header(b64 ,oribase):
    list_type = ["data:image/jpeg;base64,","data:image/png;base64,","data:image/jpg;base64,","data:image/bmp;base64,"]
    b64 = str(b64)
    b64 = b64.replace("'","")[1:]
    print(b64)
    for i in list_type:
        print(i)
        if i in oribase:
            print(i+b64)
            return i+b64

@app.route('/',methods=['GET', 'POST'])
def root():
    import time
    photo_base64 = request.json["base64"]
    print(photo_base64)
    base64_photo = photo_base64.replace("data:image/jpeg;base64,", "")
    base64_photo = base64_photo.replace("data:image/png;base64,", "")
    base64_photo = base64_photo.replace("data:image/jpg;base64,", "")
    base64_photo = base64_photo.replace("data:image/bmp;base64,", "")
    current_time = str(time.time()).split(".")[0]

    output_file = open(current_time, "w")
    output_file.write(request.json["base64"])
    service_url_dispatch = {
        "option1": train_ticket,
        "option2": bankcard,
        "option3": idcard,
        "option4": vat_invoice,
        "option5": Universal_text_recognition,
        "option6": General_text_recognition_high_precision_version,
    }[request.json["option"]]
    print(service_url_dispatch)
    return service_url_dispatch(base64_photo,photo_base64)

@app.route('/ocr')
def index():
    return render_template("index.html")


if __name__ == '__main__':
    app.run(port="5000")
